# Script pour automatiser la creation de clé SSH pour gitlab.

## Installation du script

```shell
sudo cp create-ssh-key /usr/local/bin/.
```

## Modification des droits

```shell
sudo chmod 755 /usr/local/bin/create-ssh-key
```

## Utilisation

```shell
create-ssh-key [NOM_DE_MA_CLE]
cat ~/.ssh/[NOM_DE_MA_CLE].pub
```
